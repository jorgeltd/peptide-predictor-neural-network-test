/*
Files and input channels
*/
Channel
	.fromPath('data/regression/*' )
	.into{regression1_ch; regression2_ch; regression3_ch}

Channel
	.fromPath('data/classification/*' )
	.into{classification1_ch; classification2_ch; classification3_ch}

output_dir = 'out'


/*
* 1- Encode datasets by type of problem and type of encoding (onehot, ordinal, freq)
*/
process one_hot_encoding_class {
  tag "$file"
  publishDir "${output_dir}/encode_class/onehot/", mode: "copy"

  input:
  each path(file) from classification1_ch

  output:
  path "*.npz"  into class_onehot_ch

  script:
  """
  one_hot_encoder.py -m encode -i $file -o ${file.simpleName}_onehot_encoded.npz -l response
  """
}

process ordinal_encoding_class {
  tag "$file"
  publishDir "${output_dir}/encode_class/ordinal/", mode: "copy"

  input:
  each path(file) from classification2_ch

  output:
  path "*.npz" into class_ordinal_ch

  script:
  """
  ordinal_encoder.py -m encode -i $file -o ${file.simpleName}_ordinal_encoded.npz -l response
  """
}

process freq_encoding_class {
  tag "$file"
  publishDir "${output_dir}/encode_class/freq/", mode: "copy"

  input:
  each path(file) from classification3_ch

  output:
  path "*.csv" into class_freq_ch

  script:
  """
  freq_encoder.py -i $file -o ${file.simpleName}_onehot_freq_encoded.csv -l response
  """
}

process one_hot_encoding_reg {
  tag "$file"
  publishDir "${output_dir}/encode_reg/onehot/", mode: "copy"

  input:
  each path(file) from regression1_ch

  output:
  path "*.npz" into reg_onehot_ch

  script:
  """
  one_hot_encoder.py -m encode -i $file -o ${file.simpleName}_onehot_encoded.npz -l response
  """
}

process ordinal_encoding_reg {
  tag "$file"
  publishDir "${output_dir}/encode_reg/ordinal/", mode: "copy"

  input:
  each path(file) from regression2_ch

  output:
  path "*.npz"  into reg_ordinal_ch

  script:
  """
  ordinal_encoder.py -m encode -i $file -o ${file.simpleName}_ordinal_encoded.npz -l response
  """
}

process freq_encoding_reg {
  tag "$file"
  publishDir "${output_dir}/encode_reg/freq/", mode: "copy"

  input:
  each path(file) from regression3_ch

  output:
  path "*.csv" into reg_freq_ch

  script:
  """
  freq_encoder.py -i $file -o ${file.simpleName}_freq_encoded.csv -l response
  """
}

/* 2.- Train neural networks for classification and regression*/

/*
2.A Classification
*/

process train_nn_class_freq{
  tag "$file"
  
  input:
  each path(file) from class_freq_ch

  output:
  path "*.csv" into nn_class_results_ch1

  script:
  """
  tensor_nn.py -i $file -f csv -m classification -o ${file.simpleName}_nn_results.csv
  """
}

process train_nn_class_ordinal_onehot{
  tag "$file"
  
  input:
  each path(file) from class_onehot_ch.mix(class_ordinal_ch)

  output:
  path "*.csv" into nn_class_results_ch2
  
  script:
  """
  tensor_nn.py -i $file -f npz -m classification -o ${file.simpleName}_nn_results.csv
  """
}

/*
2.B Regression
*/
process train_nn_reg_freq{
  tag "$file"
  
  input:
  each path(file) from reg_freq_ch

  output:
  path "*.csv" into nn_reg_results_ch1

  script:
  """
  tensor_nn.py -i $file -f csv -m regression -o ${file.simpleName}_nn_results.csv
  """
}

process train_nn_reg_ordinal_onehot{
  tag "$file"
  
  input:
  each path(file) from reg_onehot_ch.mix(reg_ordinal_ch)

  output:
  path "*.csv" into nn_reg_results_ch2
  
  script:
  """
  tensor_nn.py -i $file -f npz -m regression -o ${file.simpleName}_nn_results.csv
  """
}

/*
* 2.C Concat nn results
*/

process concat_nn_class_results{
  publishDir "${output_dir}/nn_tensorflow/class/", mode: "copy"

  input:
  path files from nn_class_results_ch1.mix(nn_class_results_ch2).collect()

  output:
  path "classification_resume.csv"
  
  script:
  """
  concat_tensor_results.py -i $files -o classification_resume.csv
  """
}

process concat_nn_reg_results{
  publishDir "${output_dir}/nn_tensorflow/reg/", mode: "copy"

  input:
  path files from nn_reg_results_ch1.mix(nn_reg_results_ch2).collect()

  output:
  path "regression_resume.csv"
  
  script:
  """
  concat_tensor_results.py -i $files -o regression_resume.csv
  """
}