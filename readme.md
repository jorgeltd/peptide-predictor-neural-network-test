Here are two pipelines made with nextflow
1. [workflow_1.nf](workflow_1.nf) takes datasets from [data/classification](data/classification) and 
[data/regression](data/regression), encodes them into frequency, ordinal and one-hot encodings 
and run a tensorflow neural network with the datasets encoded.
2. [workflow_2.nf](workflow_2.nf) takes datasets from [data/dataset_nn/](data/dataset_nn) that are already pre processed and
splited into training and testing and pass them throughout a tensorflow neural network 

## Requirements
* Install [miniconda](https://docs.conda.io/en/latest/miniconda.html),  use `conda config --set auto_activate_base false`
to deactivate the (base) environment.

## How to run
Create conda env

```shell script
conda env create -f env.yml
```

Activate conda env
```shell script
conda activate encoder_nn
```

Run one of the two workflows

```shell script
nextflow run <script> -resume -process.cpus=n
```
Where 
* `<script>`: is one of the two workflows.
* `-resume`: allows to resuse files already processed that are saved in the work directory.
* `-process.cpus=n`: use `n` cores in the workflow.

### Output
Results of the pipelines are gonna be in the directory "out".