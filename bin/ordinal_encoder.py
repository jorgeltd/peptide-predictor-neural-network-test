#!/usr/bin/env python
import pandas as pd
import argparse
import re
import numpy as np

# For ordinal encoding, position 0 of alphabet is '-' for padding or to replace extra characters
ALPHABET = sorted(['A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V', '-'])

# 20 aminoacid, padding character doesnt count, also padding character value and index is 0
ORDINAL_STEP = 1 / 20


def encode_seq(seq):
    seq = seq.upper()
    seq = re.sub(f"[^{''.join(ALPHABET)}]", '-', seq)

    # For 20 aminoacid, ordinal encoder has a step of 0.05. 0 is reserved for '-' character
    seq_encoded = [ALPHABET.index(amino) * ORDINAL_STEP for amino in seq]

    return np.array(seq_encoded)


def encode_data(series):
    # Padding before encoding
    max_seq_len = series.str.len().max()
    series = series.apply(lambda x: x.ljust(max_seq_len, '-'))

    arr = np.empty((0, max_seq_len))

    for idx, seq in series.items():
        data = encode_seq(seq).reshape((1, max_seq_len))
        arr = np.append(arr, data, axis=0)

    return arr


def encode_call(args):
    print(f"Ordinal encoding file {args.input}")
    df = pd.read_csv(args.input)

    encoded_data = encode_data(df['sequence'])
    index = df.index.to_numpy()
    labels = df[args.label].to_numpy()

    np.savez(args.output, labels=labels, data=encoded_data, index=index)
    print('Done!')


def decode_call(args):
    return None


def parse_arguments():
    """Encode protein sequence to ordinal encoding with zero padding

    """

    parser = argparse.ArgumentParser(
        "Encode protein sequence to ordinal encoding with zero padding"
    )

    parser.add_argument(
        '-m',
        '--mode',
        choices=['encode', 'decode'],
        help='script mode'
    )

    parser.add_argument(
        "-i",
        "--input",
        action="store",
        required=True,
        help="csv file with sequences",
    )

    parser.add_argument(
        "-o",
        "--output",
        action="store",
        required=True,
        help="path for output",
    )

    parser.add_argument(
        "-l",
        "--label",
        action="store",
        required=True,
        help="column name of csv file with the label of the dataset",
    )

    return parser.parse_args()


if __name__ == '__main__':
    arg = parse_arguments()

    if arg.mode == 'encode':
        encode_call(arg)
    elif arg.mode == 'decode':
        decode_call(arg)
    else:
        print('no mode selected')
