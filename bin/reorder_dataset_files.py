#!/usr/bin/env python
import argparse
from os import walk, path
from shutil import copyfile


def main():
    """
    Script para reorganizar archivos de de los datasets dbp y solub
    :return:
    """
    args = parse_arguments()

    for root, dirs, files in walk(args.input_dir):
        for name in files:
            if "dataset.csv" in name:
                dataset = root.split('/')[-3]
                prop = root.split('/')[-1]
                data_type = name.replace('_dataset.csv', '.csv')
                cp_dest = path.join(args.output_dir, "_".join([dataset, prop, data_type]))
                copyfile(path.join(root, name), cp_dest)
                print(path.join(root, name), cp_dest)


def parse_arguments():
    """Parse arguments
    """

    parser = argparse.ArgumentParser(
        "Script for concat csv files with tensorflow results"
    )

    parser.add_argument(
        "-i",
        "--input-dir",
        action="store",
        required=True,
        help="input dir with dataset files",
    )

    parser.add_argument(
        "-o",
        "--output-dir",
        action="store",
        required=True,
        help="path for output dir with files ordered",
    )

    return parser.parse_args()


if __name__ == '__main__':
    main()
