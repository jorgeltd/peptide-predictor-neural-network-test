#!/usr/bin/env python
import argparse
import pandas as pd


def main():
    args = parse_arguments()

    df_list = []
    for f in args.input:
        df_list.append( pd.read_csv(f, sep=','))

    df_concat = pd.concat(df_list)
    df_concat.to_csv(args.output, index=False, sep=',', float_format='%.4f')


def parse_arguments():
    """Parse arguments
    """

    parser = argparse.ArgumentParser(
        "Script for concatenation of csv files with tensorflow results"
    )

    parser.add_argument(
        "-i",
        "--input",
        action="store",
        required=True,
        nargs='+',
        help="input data file",
    )

    parser.add_argument(
        "-o",
        "--output",
        action="store",
        required=True,
        help="path for output",
    )

    return parser.parse_args()


if __name__ == '__main__':
    main()
