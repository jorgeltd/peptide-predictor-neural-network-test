#!/usr/bin/env python
import pandas as pd
import argparse
import numpy as np

ALPHABET = sorted(['A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V'])


def encode_amino(amino):

    try:
        index = ALPHABET.index(amino.upper())
    except ValueError:
        print(f"Character '{amino}' is not a valid aminoacid")
        return None

    vector = np.zeros(len(ALPHABET))
    vector[index] = 1

    return vector


def encode_sequence(seq, pad=0, flatten=False):

    if type(seq) != str:
        print("Error, seq is not a string")
        return None

    array = np.array([encode_amino(x) for x in seq if x.upper() in ALPHABET])

    if pad > 0:
        array = np.append(array, np.zeros((pad, len(ALPHABET))), axis=0)

    if flatten:
        array = array.flatten()

    return array


def encode_data(series):
    max_seq_len = series.str.len().max()
    row_len = max_seq_len * len(ALPHABET)
    arr = np.empty((0, row_len))

    for idx, seq in series.items():

        if idx % 1000 == 0:
            print(idx)

        pad = max_seq_len - len(seq)
        data = encode_sequence(seq, pad=pad, flatten=True).reshape((1, row_len))
        arr = np.append(arr, data, axis=0)

    return np.int_(arr)


def decode_data(encoded_data):

    n_rows = encoded_data.shape[0]
    n_cols = encoded_data.shape[1]
    decoded_data = []

    for i in range(0, n_rows):

        data = encoded_data[i, :].reshape(int(n_cols/len(ALPHABET)), len(ALPHABET))
        row_idx, col_idx = np.where(data == 1)
        seq = "".join([ALPHABET[idx] for idx in col_idx])
        decoded_data.append(seq)

    return decoded_data


def encode_call(args):

    print(f"One-hot encoding file {args.input}")
    df = pd.read_csv(args.input)

    # Delete character outside of alphabet
    df['sequence'] = df['sequence'].replace(f"[^{''.join(ALPHABET)}]", '', regex=True)
    encoded_data = encode_data(df['sequence'])
    index = df.index.to_numpy()
    labels = df[args.label].to_numpy()

    np.savez(args.output, labels=labels, data=encoded_data, index=index)
    print('Done!')


def decode_call(args):
    print(f"One-hot decoding file {args.input}")

    with np.load(args.input) as data:
        labels = data['labels']
        encoded_data = data['data']
        index = data['index']

        decoded_data = decode_data(encoded_data)

        df = pd.DataFrame({'label': labels, 'sequence': decoded_data}, index=index)
        df.to_csv(args.output, sep=',', index=False)

    print('Done!')


def parse_arguments():
    """Encode protein sequence to one-hot encoding with zero padding

    """

    parser = argparse.ArgumentParser(
        "Encode protein sequence to one-hot encoding with zero padding"
    )

    parser.add_argument(
        '-m',
        '--mode',
        choices=['encode', 'decode'],
        help='script mode'
    )

    parser.add_argument(
        "-i",
        "--input",
        action="store",
        required=True,
        help="csv file with sequences",
    )

    parser.add_argument(
        "-o",
        "--output",
        action="store",
        required=True,
        help="path for output",
    )

    parser.add_argument(
        "-l",
        "--label",
        action="store",
        required=True,
        help="column name of csv file with the label of the dataset",
    )

    return parser.parse_args()


if __name__ == '__main__':
    arg = parse_arguments()

    if arg.mode == 'encode':
        encode_call(arg)
    elif arg.mode == 'decode':
        decode_call(arg)
    else:
        print('no mode selected')
