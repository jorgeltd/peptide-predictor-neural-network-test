#!/usr/bin/env python
import argparse
import pandas as pd
import numpy as np
import tensorflow as tf
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from tensorflow import keras
from tensorflow.keras import layers


def main():
    args = parse_arguments()

    # Case 1: only 1 dataset file input
    if args.input and args.training_dataset is None and args.test_dataset is None:
        # Load dataset
        x_train, x_test, y_train, y_test = preproc_dataset(args.input, args.format, args.mode)
        file = args.input

    # Case 2: testing and training dataset are provided
    elif args.training_dataset and args.test_dataset and args.input is None:
        # Load dataset
        x_train, x_test, y_train, y_test = preproc_split_dataset(args.training_dataset, args.test_dataset, args.format, args.mode)
        file = args.training_dataset

    elif args.input and (args.training_dataset or args.test_dataset):
        print("Combination of -i and -t or -e not valid")
        exit(-1)

    # Train tensorflow neural networks
    if args.mode == 'classification':
        model, history = run_classification_model(x_train, y_train, x_test, y_test, name=file, output=args.output)

    elif args.mode == 'regression':
        model, history = run_regression_model(x_train, y_train, x_test, y_test, name=file, output=args.output)


def preproc_split_dataset(train_f_path, test_f_path, f_format, mode):
    """
    Load dataset files splited into training and testing
    :param train_f_path: path of training dataset file
    :param test_f_path: path of testing dataset file
    :param f_format: format of dataser {csv,npz}
    :param mode: type of problem {classification,regression}
    :return: x_train, y_train, x_test, y_test datasets
    """
    assert mode == 'regression' or mode == 'classification', 'wrong mode only {classification, regression}'

    # Load dataset
    x_train, y_train = load_data(train_f_path, f_format)
    x_test, y_test = load_data(test_f_path, f_format)

    if mode == 'classification':
        # Transform string labels to int
        le = preprocessing.LabelEncoder()
        le.fit(np.concatenate([y_train, y_test]))
        y_train = le.transform(y_train)
        y_test = le.transform(y_test)

    return x_train, x_test, y_train, y_test


def preproc_dataset(f_path, f_format, mode):
    """
    Load dataset file and split into training and testing dataset
    :param f_path: path of dataset file
    :param f_format: format of dataset {csv, npz}
    :param mode: type of problem {classification,regression}
    :return: x_train, y_train, x_test, y_test datasets
    """

    assert mode == 'regression' or mode == 'classification', 'wrong mode only {classification, regression}'

    # Load dataset and remove null values
    data, raw_labels = load_data(f_path, f_format)
    nan_labels = pd.isna(raw_labels)
    raw_labels = raw_labels[~nan_labels]
    data = data[~nan_labels, :]

    if mode == 'classification':
        # Transform string labels to int
        le = preprocessing.LabelEncoder()
        le.fit(raw_labels)
        labels = le.transform(raw_labels)

    elif mode == 'regression':
        labels = raw_labels

    # Split dataset into training and test
    x_train, x_test, y_train, y_test = train_test_split(data, labels, test_size=0.3)

    return x_train, x_test, y_train, y_test


def run_regression_model(x_train, y_train, x_test, y_test, name='', output=None, epochs=50, batch_size=256):
    """
    Run a tensorflow regression model
    :param x_train: dataset with features for training
    :param y_train: dataset with response of features for training
    :param x_test: dataset with features for testing
    :param y_test: dataset with response of features for testing
    :param name: name of dataset for stats resume
    :param output: if set, path for csv file with stats resume
    :param epochs: number of epochs in training phase
    :param batch_size: size of batches for training phase
    :return: trained model and history
    """

    # Some stats
    assert x_train.shape[1] == x_test.shape[1]
    n_features = x_train.shape[1]
    n_dataset = x_train.shape[0] + x_test.shape[0]
    mean_y = np.mean(np.concatenate([y_train, y_test]))
    std_y = np.std(np.concatenate([y_train, y_test]))

    # Create model
    model = build_regression_nn(n_features)

    # Fit data
    history = model.fit(x_train,
                        y_train,
                        epochs=epochs,
                        batch_size=batch_size,
                        validation_split=0.2,
                        verbose=0
                        )
    # Test data
    test_loss, test_mae, test_mse = model.evaluate(x_test, y_test, verbose=2)

    train_loss = history.history['loss'][-1]
    train_mae = history.history['mae'][-1]
    train_mse = history.history['mse'][-1]

    if output and output != '':
        # Save results
        df_results = results_to_df(dataset=name,
                                   problem='regression',
                                   n_dataset=n_dataset,
                                   n_features=n_features,
                                   train_loss=train_loss,
                                   train_mae=train_mae,
                                   train_mse=train_mse,
                                   test_loss=test_loss,
                                   test_mae=test_mae,
                                   test_mse=test_mse,
                                   mean_y=mean_y,
                                   std_y=std_y,
                                   epochs=epochs,
                                   batch_size=batch_size
                                   )
        df_results.to_csv(output, index=False, float_format='%0.4f', sep=',')

    return model, history


def run_classification_model(x_train, y_train, x_test, y_test, name='', output=None, epochs=50, batch_size=256):
    """
     Run a tensorflow classification model
    :param x_train: dataset with features for training
    :param y_train: dataset with response of features for training
    :param x_test: dataset with features for testing
    :param y_test: dataset with response of features for testing
    :param name: name of dataset for stats resume
    :param output: if set, path for csv file with stats resume
    :param epochs: number of epochs in training phase
    :param batch_size: size of batches for training phase
    :return: trained model and history
    """
    # Some stats
    assert x_train.shape[1] == x_test.shape[1]
    n_features = x_train.shape[1]
    n_classes = len(label_count(np.concatenate([y_train, y_test])))
    n_dataset = x_train.shape[0] + x_test.shape[0]

    # Create model
    model = build_classification_nn(n_features, n_classes)

    # Fit data
    history = model.fit(x_train,
                        y_train,
                        verbose=0,
                        epochs=epochs,
                        validation_split=0.2,
                        batch_size=batch_size)
    # Test data
    test_loss, test_acc = model.evaluate(x_test, y_test, verbose=2)
    train_acc = history.history['accuracy'][-1]

    if output and output != '':
        # Save results
        df_results = results_to_df(name=name,
                                   problem='classification',
                                   n_dataset=n_dataset,
                                   n_features=n_features,
                                   n_classes=n_classes,
                                   train_acc=train_acc,
                                   test_acc=test_acc,
                                   epochs=epochs,
                                   batch_size=batch_size
                                   )
        df_results.to_csv(output, index=False, float_format='%0.4f', sep=',')

    return model, history


def results_to_df(**kwargs):
    """
    Create a df with parameters of interest
    :param kwargs: key=value of parameter
    :return:
    """
    results_dic = {}
    for key in kwargs:
        results_dic[key] = [kwargs[key]]

    return pd.DataFrame(results_dic)


def build_classification_nn(n_features, n_classes):
    """
    Create a classification nn with tensorflow
    :param n_features: len of feature vector
    :param n_classes: number of classes in classification
    :return: tensorflow model
    """
    inputs = keras.Input(shape=(n_features,), name="features")
    x = layers.Dense(64, activation="relu", name="hidden_1")(inputs)
    x = layers.Dense(64, activation="relu", name="hidden_2")(x)
    outputs = layers.Dense(n_classes, activation="softmax", name="predictions")(x)

    model = keras.Model(inputs=inputs, outputs=outputs)

    model.compile(optimizer='adam',
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])

    return model


def build_regression_nn(n_features):
    """
    Create a regression nn with tensorflow
    :param n_features: len of feature vector
    :return: tensorflow model
    """
    inputs = keras.Input(shape=(n_features,), name="features")
    x = layers.Dense(64, activation="relu", name="hidden_1")(inputs)
    x = layers.Dense(64, activation="relu", name="hidden_2")(x)
    outputs = layers.Dense(1, name="predictions")(x)

    model = keras.Model(inputs=inputs, outputs=outputs)

    optimizer = tf.keras.optimizers.RMSprop(0.001)

    model.compile(loss='mse',
                  optimizer=optimizer,
                  metrics=['mae', 'mse'])

    return model


def label_count(labels):
    """
    Check count of classes in label array
    :param labels: array with classes
    :return: dict with count of unique classes
    """
    unique, counts = np.unique(labels, return_counts=True)
    return dict(zip(unique, counts))


def load_data(input_path, f_format):
    """
    Load data files for nn
    :param input_path: path of input file
    :param f_format: format of the file {csv, npz}
    :return: data and labels arrays
    """

    if f_format == 'csv':
        csv_file = pd.read_csv(input_path, sep=',')
        data = csv_file.iloc[:, :-1].to_numpy()
        labels = csv_file.iloc[:, -1].to_numpy()

        return data, labels

    elif f_format == 'npz':
        npz_file = np.load(input_path, allow_pickle=True)

        return npz_file['data'], npz_file['labels']


def parse_arguments():
    """Parse input arguments
    """

    parser = argparse.ArgumentParser(
        "Script for training a neural network made with tensor flow"
    )

    parser.add_argument(
        "-i",
        "--input",
        action="store",
        help="input data file",
    )

    parser.add_argument(
        "-e",
        "--training-dataset",
        action="store",
        help="training dataset path",
    )

    parser.add_argument(
        "-t",
        "--test-dataset",
        action="store",
        help="test dataset path",
    )

    parser.add_argument(
        "-f",
        "--format",
        choices=['csv', 'npz'],
        action="store",
        required=True,
        help="format of input file",
    )

    parser.add_argument(
        "-m",
        "--mode",
        choices=['classification', 'regression'],
        action="store",
        required=True,
        help="Type of problem",
    )

    parser.add_argument(
        "-o",
        "--output",
        action="store",
        required=True,
        help="path for output resume csv table",
    )

    parser.add_argument(
        "-s",
        "--save-model",
        action="store",
        help="output path for trained model",
    )

    return parser.parse_args()


if __name__ == '__main__':
    main()
