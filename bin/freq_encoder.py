#!/usr/bin/env python
import pandas as pd
import argparse

ALPHABET = sorted(['A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V'])


def encode_seq(seq):
    assert type(seq) == str, 'seq is not a string'

    seq_len = len(seq)
    freq_count = []
    for amino in ALPHABET:

        amino_count = seq.count(amino)
        freq_count.append(amino_count/seq_len)

    return freq_count


def encode_data(series):

    data = []
    for idx, seq in series.items():
        data.append(encode_seq(seq))

    return pd.DataFrame(data, columns=ALPHABET)


def encode_call():
    args = parse_arguments()

    print(f"Frequency encoding file {args.input}")
    df = pd.read_csv(args.input)
    encoded_data = encode_data(df['sequence'])
    encoded_data['label'] = df[args.label]

    encoded_data.to_csv(args.output, sep=',', float_format='%.4f', index=False)

    print('Done!')


def parse_arguments():
    """Encode protein sequence to amino frequency encoding

    """

    parser = argparse.ArgumentParser(
        "Encode protein sequence to amino frequency encoding"
    )

    parser.add_argument(
        "-i",
        "--input",
        action="store",
        required=True,
        help="csv file with sequences",
    )

    parser.add_argument(
        "-o",
        "--output",
        action="store",
        required=True,
        help="path for output",
    )

    parser.add_argument(
        "-l",
        "--label",
        action="store",
        required=True,
        help="column name of csv file with the label of the dataset",
    )

    return parser.parse_args()


if __name__ == '__main__':
    encode_call()
