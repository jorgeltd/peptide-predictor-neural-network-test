/*
Files and input channels
*/
Channel
    .fromFilePairs('data/dataset_nn/dbp/*_{testing,training}.csv')
    .set { dbp_ch }

Channel
    .fromFilePairs('data/dataset_nn/solub/*_{testing,training}.csv')
    .set { solub_ch }

output_dir = 'out'

/*1.- dbp classification dataset*/
process train_class_nn {
  
  input:
  set id, file(datasets) from dbp_ch

  output:
  path "*.csv" into class_resume_ch

  script:
  testing = datasets[0]
  training = datasets[1]

  """
  tensor_nn.py -e $training -t $testing -f csv -m classification -o ${id}_nn_results.csv
  """
}

process concat_nn_class_results{
  publishDir "${output_dir}/dataset_david/dbp/", mode: "copy"

  input:
  path files from class_resume_ch.collect()

  output:
  path "*.csv"
  
  script:
  """
  concat_tensor_results.py -i $files -o dbp_resume.csv
  """
}

/*1.- solub classification dataset*/
process train_reg_nn {
  
  input:
  set id, file(datasets) from solub_ch

  output:
  path "*.csv" into reg_resume_ch

  script:
  testing = datasets[0]
  training = datasets[1]

  """
  tensor_nn.py -e $training -t $testing -f csv -m regression -o ${id}_nn_results.csv
  """
}

process concat_nn_reg_results{
  publishDir "${output_dir}/dataset_david/solub/", mode: "copy"

  input:
  path files from reg_resume_ch.collect()

  output:
  path "*.csv"
  
  script:
  """
  concat_tensor_results.py -i $files -o solub_resume.csv
  """
}